import '../index.html';
// le chemin relatif pointe déjà sur le dossier node_modules
import 'mapbox-gl/dist/mapbox-gl.css';
import '../style.css';
import './orgues.jpeg';
import appConfig from '../../app.config.js';
import { EventClass } from './EventClass.js';
import { RefreshClass } from './RefreshClass.js';

const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');

/* ----------------------------------------------------------------------------

1) TODO: Faire le test d'intégrité des données du formulaire.
2) TODO: Déplacer le traitement du bouton "refresh" de App.js vers RefreshClass.js

----------------------------------------------------------------------------- */

class App { // Déclaration des propriétés de la classe App

    title;
    description;
    startDate;
    endDate;
    longitude;
    latitude;

    eventArray = []; // Tableau des événements au format json

    Markers = []; // Tableau contenant les objets Marker
    map; 

    storageName = appConfig.localStorageName; // Nom nu localstorage
    defaultEvents = appConfig.defaultEvents; // évenements par défaut

    start() { // Fonction raccourci pour éviter de multiples querySelector

        function ⵣ( domElement ) {
            return document.querySelector( domElement );
        }
            
        this.initMapBox(); // Initialisation de l'API mapbox
        this.loadAllEvents(); // Chargement des événements

// -----------------------------------------------------------------------------------------

        this.map.on('click', (e) => { // Hydratation des champs et propriétés longitude et latitude lors du clic sur la carte

            this.longitude = ⵣ('#inpEventLongitude').value = e.lngLat.lng;
            this.latitude = ⵣ('#inpEventLatitude').value = e.lngLat.lat;
        });

// -----------------------------------------------------------------------------------------

        ⵣ('#btnSaveEvent').addEventListener('click', () => { 
        // Hydratation des propriétés lors de l'enregistrement d'un événement
            let thisEvent = {

                title: this.title = ⵣ('#inpEventTitle').value,
                description: this.description = ⵣ('#inpEventDescription').value,
                startDate: this.startDate = ⵣ('#inpEventStart').value,
                endDate: this.endDate = ⵣ('#inpEventEnd').value,
                longitude: this.longitude,
                latitude: this.latitude
            };

            this.eventArray.push( thisEvent ); // Stockage de l'événement au format json dans le tableau eventArray
            
            this.setLocalStorage( this.eventArray ); // Stockage des événements dans le localStorage
            
            this.refreshMap( thisEvent ); // Affichage des punaises sur la carte
        });

// -----------------------------------------------------------------------------------------

        ⵣ('#btnRefresh').addEventListener('click', () => {
            // Rafraichissement de l'affichage de la carte 
            this.refreshMap( '', true ); // Suppression de toutes les punaises
            setTimeout( () => { this.loadAllEvents(); }, 100 ); // Ré-affichage de celles-ci 1/10ème de seconde plus tard        
        });
    }

// -----------------------------------------------------------------------------------------

    initMapBox() { // Méthode d'initialisation de mapbox

        console.log( 'Application started' );
        // Alert javascript lors du click sur la barre inférieure de mapbox
        const htmlContent = `<a href="javascript:alert( document.querySelector('#bob').innerText);" 
        id="bob" style="background-color:white">&nbsp;❤️❤️❤️&nbsp;Mon super site de la mort qui tue !!!&nbsp;🤣🤣🤣</a>`;
        // Clé d'enregistrement de l'API
        mapboxgl.accessToken = 'pk.eyJ1IjoiaGE2NiIsImEiOiJja25mb2d6dXoyZ2tiMm9tcTk0aTMyYjg0In0.WBeRyYR3vUl0holdQ5sn6A';
        
        this.map = new mapboxgl.Map({
            container: 'map',
            center: { lng: 2.77, lat: 42.63 },
            zoom: 9,
            customAttribution: htmlContent,
            // maxBounds: [[2.7625387, 42.6874962], [2.8178099, 42.6774791]],
            style: 'mapbox://styles/mapbox/satellite-streets-v11'
        });
        // Ajout du bouton de zoom
        const zoomCtrl = new mapboxgl.NavigationControl();
        this.map.addControl( zoomCtrl );
        // Ajout du bouton "refresh"
        const refreshCtrl = new RefreshClass( this.map );
        this.map.addControl( refreshCtrl, 'top-left');
    }

// -----------------------------------------------------------------------------------------

    loadAllEvents() { // Méthode de chargement des événenements depuis le localStorage

        if( this.getLocalStorage() === null ) { // Si pas d'événements enregistrés, hydratation du localStorage avec les
        // événenements par défaut
            this.setLocalStorage(this.defaultEvents);
        }

        let storedEvents;
        let storageContent = this.getLocalStorage(); // Chargement du localStorage hydraté

        try {
            storedEvents = JSON.parse( storageContent ); // Hydratation de storedEvents
        }

        catch( error ) { // Si localStorage corrompu, effacement de celui-ci
            localStorage.removeItem( this.storageName );
            return;
        }
 
        for( let thisEvent of storedEvents ) {

            // Affectation des propriétés longitude et latitudes pour corriger un bug de mapbox
            if( isNaN( thisEvent.longitude ) ) thisEvent.longitude = 2.77; 
            if( isNaN( thisEvent.latitude ) ) thisEvent.latitude = 42.63;

            this.eventArray.push( thisEvent ); // Stockage de l'événement au format json dans le tableau eventArray

            this.refreshMap( thisEvent ); // Affichage des punaises sur la carte
        }
    }

    // -----------------------------------------------------------------------------------------

    toJSON() { // Méthode appellée implicitement lors de l'appel à JSON.parse
        
        return {
            title: this.title,
            description: this.description,
            startDate: this.startDate,
            endDate: this.endDate,
            longitude: this.longitude,
            latitude: this.latitude
        }
    }

    // -----------------------------------------------------------------------------------------

    refreshMap( thisEvent, erase = false ) { // Méthode raffraichissement de l'affichage sur la carte

        if( erase ) { // si erase == true alors on supprime, toutes les punaises

            for( let thisMarker of this.Markers ) {
                
                thisMarker.remove();
            }

            this.Markers = []; // On vide le tableau d'objets Markers
            this.eventArray = [];  // On vide le tableau d'évenements au format json
        } else {
            // sinon on ajoute une punaise sur la carte
            const newEvent = new EventClass( thisEvent ).Marker.addTo( this.map );
            this.Markers.push( newEvent ); // On suvegarde la punaise dans le tableau d'objets Markers
        }
    }

// -----------------------------------------------------------------------------------------

    getLocalStorage(storageName = this.storageName) { // Méthode de lecture de localStorage
        return localStorage.getItem( storageName )
    }

// -----------------------------------------------------------------------------------------

    setLocalStorage( thisJsonEvent, storageName = this.storageName ) { // Méthode de sauvegarde dans localStorage
        localStorage.setItem( storageName, JSON.stringify( thisJsonEvent ) );
    }
}

const instance = new App(); // Création de l'instance unique de classe App
export default instance;
