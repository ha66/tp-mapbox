const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js'); //, Import de la librairie js mapbox

export class EventClass { // Création de la class EventClass

    title;
    description;
    startDate;
    endDate;

    longitude;
    latitude;

    Marker; // Tableau d'objets Marker

    constructor(newEvent) { // Hydratation des propriétés de la classe avec le nouvel événement
        
        this.title = newEvent.title;
        this.description = newEvent.description;
        this.startDate = newEvent.startDate;
        this.endDate = newEvent.endDate;
        this.longitude = newEvent.longitude;
        this.latitude = newEvent.latitude;

        this.createMarker(); // Appel de la méthode createMarker() qui crée la punaise
    }

// -----------------------------------------------------------------------------------------

    createMarker() { // Création de la punaise       
       
        this.Marker = new mapboxgl.Marker({ color: this.getColorMarker(this.startDate) }); // Avec sa couleur en fonction de la date

        this.Marker.setLngLat({ lng: this.longitude, lat: this.latitude }); // Hydratation des longitude et latitude

        // Création de l'infobulle sous le curseur avec les dates en français de l'événement 
        this.Marker.getElement().title = `${this.title}\nDu : ${this.getFrenchDate(this.startDate)}\nAu : ${this.getFrenchDate(this.endDate)}`;
        
        const myPopup = new mapboxgl.Popup({ closeButton: false }); // Instanciation de la popup au dessus de la punaise      
        myPopup.setHTML(this.getPopupContent()); // Remplissage de celle-ci avec son contenu html

        this.Marker.setPopup( myPopup ); // Et affichage de la popup
    }

// -----------------------------------------------------------------------------------------

    getColorMarker(thisDate) { // Méthode de définition de la couleur de la punaise en fonction de la date de l'événement

        const eventDate = new Date( thisDate ); // Création typée de la date de l'événement
        
        const delay = eventDate.getTime() - new Date().getTime(); // Délai entre la date de l'événement et la date actuelle
        // par soustraction des 2 timestamp

        const threeDays = 1000 * 60 * 60 * 24 * 3; // Nombre de millisecondes en 3 jours
        
        let color = "red"; // Couleur rouge par défaut de la punaise (cas où l'événement est passé)
        
        if( delay > 0 && delay <= threeDays ) color = "orange"; // Si le délai est compris entre maintenant et 3 jours
        // la couleur de la punaise est orange
        if( delay > threeDays ) color = "green"; // Si le délai avant l'événement est supérieur 3 jours 
        // la couleur de la punaise est verte        
        return color; // On renvoie la couleur de la punaise
    }

// -----------------------------------------------------------------------------------------

    getFrenchDate(date = this.startDate) { // Inversion de année / mois / jour pour l'affichage au format français
        return date.split('-').reverse().join('/');
    }

// -----------------------------------------------------------------------------------------

    getPopupContent() { // Méthode de création du HTML pour la popup

        const htmlPopupContent = `
        
        <div class="popupContent">
            <div>
                <h2>${this.title}</h2>
            </div>
            <div>
                ${this.description}<br>
            </div>
            <div>
                Du ${this.getFrenchDate(this.startDate)}
                au ${this.getFrenchDate(this.endDate)}
            </div><br>
            <div>
                <span>GPS :</span><br>
                Longitude : ${this.longitude}
            </div>
            <div>
                Latitude : ${this.latitude}
            </div><img src="./orgues.jpeg">
        </div>
        `;

        return htmlPopupContent; // On renvoie le code HTML à la popup
    }
}


