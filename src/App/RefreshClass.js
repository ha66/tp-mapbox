const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');

export class RefreshClass {

    onAdd(map) {
    this._map = map;
    this._container = document.createElement('div');
    this._container.className = 'mapboxgl-ctrl';
    this._container.innerHTML = '<button id="btnRefresh">🔄&nbsp;&nbsp;&nbsp;Actualiser</button>'; 
    //this._container.addEventListener('click',this.onClick)
    return this._container;
    }
     
    onRemove() {
    this._container.parentNode.removeChild(this._container);
    this._map = undefined;
    }
}
