export default {
    
    appName: 'tp-mapbox',

    localStorageName: 'eventList',

    defaultEvents: 
    [
        {
        "title": "#BogossDu66 en concert au Boulou",
        "description": "Vous êtes invités à son concert en compagnie de son voisin 'chanteur' et de sa voisine vraiment Chti !!!<br>Venez nombreux. 1€ le Pastis.",
        "startDate": "2021-04-30",
        "endDate": "2021-05-08",
        "longitude": "2.83382874553789",
        "latitude": "42.5409352272494"
        },
        {
        "title": "Tournoi des cerises à Céret",
        "description": "Cette fête du basket vient clôturer la saison lors d'un tournoi international amateur. Au programme, basket évidemment mais aussi ambiance festive et conviviale durant un weekend avec de nombreuses animations.",
        "startDate": "2021-05-29",
        "endDate": "2021-05-30",
        "longitude": "2.75026201509019",
        "latitude": "42.5063675066617"
        },
        {
        "title": "Concours de pétanque au lac de Villeneuve",
        "description": "Si tu es amateur d'apéro, de boules et cochonet, mais aussi de barbecue, ce rdv est fait pour toi.<br>Bob Ricard, marcel, shorty, tong et abdos Kroenenbourg obligatoires.<br>Oui, tu peux ramener ton cubi !",
        "startDate": "2021-05-08",
        "endDate": "2021-05-08",
        "longitude": "2.90276196970692",
        "latitude": "42.6520438049074"
        },
        {
        "title": "Concours de pêche à Port-Vendres",
        "description": "Pour les amateurs de poulpes, congres, rascasses et autres daurades.",
        "startDate": "2021-03-08",
        "endDate": "2021-03-08",
        "longitude": "3.09647812827376",
        "latitude": "42.5377571226312"
        }
    ]
}